import urllib.request
import re
from bs4 import BeautifulSoup
import requests
from difflib import SequenceMatcher
from collections import defaultdict, OrderedDict
import dryscrape
import time

def extract(vendor, product_name) :

	d = defaultdict(list)
	di = OrderedDict()
	if vendor == "amazon" : 

		product = product_name.replace(" ", "+")
		product = product.lower()
		url1 = 'http://www.amazon.in/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=' + product
		response = requests.get(url1, headers={'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'})
		soup = BeautifulSoup(response.content)
		rs = soup.findAll('li', attrs = { 'class' : 's-result-item celwidget '})
		if len(rs) == 0:
			rs = soup.findAll('li', attrs = { 'class' : 's-result-item s-result-card-for-container a-declarative celwidget '})
#		print(len(rs))
		j = 0

		for i in range(res) :

			bs = soup.find('li', attrs = {'id' : "result_"+ str(i) } )
			try :
				name = bs.find('h2')
				name = name.text
			except :
				name = "None"
			if product_name in name.lower() : 
				key = str(j)
				d[key].append(name)
			
				try :
					rate  = bs.find('span', attrs = { 'class' : 'a-color-price'})
					rate = rate.text.strip()
				except AttributeError : 
					rate = 0
				d[key].append(rate)
			
				try :
					rating = bs.find('span', attrs = { 'data-action' : 'a-popover', 'class' : 'a-declarative'})
					rat = rating.text
					r = re.findall("\d+[\.]?\d*", rat)
					
				except AttributeError :
					r = [0, 0]
				
				d[key].append(float(r[0]))
				j = j + 1
		
	elif vendor == "snapdeal" :

		product = product_name.replace(" ", "%20")
		product = product.lower()
		url1 = "https://www.snapdeal.com/search?keyword=" + product + "&santizedKeyword=&catId=&categoryId=0&suggested=false&vertical=&noOfResults=20&searchState=&clickSrc=go_header&lastKeyword=&prodCatId=&changeBackToAll=false&foundInAll=false&categoryIdSearched=&cityPageUrl=&categoryUrl=&url=&utmContent=&dealDetail=&sort=rlvncy"
		response = requests.get(url1, headers={'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'})
		soup = BeautifulSoup(response.content)
		j = 0
		
		for i in range(20) :
			bs = soup.find('div', attrs = {'data-js-pos' : str(i) } )

			try :
				name = bs.find('p', attrs = {'class' : 'product-title'})
				name = name.text
			except :
				time.sleep(10)
				response = requests.get(url1, headers={'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'})
				soup = BeautifulSoup(response.content)
				bs = soup.find('div', attrs = {'data-js-pos' : str(i) } )
				try :
					name = bs.find('p', attrs = {'class' : 'product-title'})
					name = name.text
				except :
					name = "None"
			name = name.lower()
			if product_name in name : 

				key = str(j)
				
				d[key].append(name)

				try :
					rate  = bs.find('span', attrs = { 'class' : 'lfloat product-price'})
					rate = rate.text.strip()
				except AttributeError : 
					rate = 0
			
				d[key].append(rate)
			
				try :
					rating = bs.find('div',style = True )
					rat = rating['style']
					r = re.findall("\d+[\.]?\d*", rat)
					ratin = (float(r[0])/100)*5
				except TypeError :
					ratin = 0
			
				d[key].append(ratin)
				j = j+1

	elif vendor == "koovs" :

		product = product_name.replace(" ", "-")
		product = product.lower()
		url1 = "http://www.koovs.com/" + product
		response = requests.get(url1, headers={'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'})
		soup = BeautifulSoup(response.content)
		j = 0
		
		for i in range(1,20) :
			bs = soup.find('div', attrs = {'unbxdparam_prank' : str(i) } )
			try :
				name = bs.find('div', attrs = {'class' : "prodDescp"})
				name = name.text.strip()
				n = re.match( r'(\w+\s)+', name)
				name = n.group()
			except :
				name = 'None'

			if product_name in name.lower() :

				key = str(j)	
				d[key].append(name.strip())

				try :
					rate  = bs.find('span', attrs = { 'class' : 'prodPrice'})
					rate = rate.text.strip()
				except AttributeError : 
					rate = 0
				d[key].append(rate)
		
				ratin = 0
				d[key].append(ratin)
				j = j+1		

	elif vendor == "myntra" :

		product = product_name.replace(" ", "-")
		product = product.lower()
		url1 = "http://www.myntra.com/" + product + "?userQuery=true"
		response = requests.get(url1, headers={'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'})
		soup = BeautifulSoup(response.content, 'html.parser')
		bs = soup.findAll("script") #, attrs = { 'type' : 'text/javascript'})
		
#		data = bs[3]
		price = re.findall(r'\"discounted_price\"\:\d+', str(bs))
		name = re.findall(r'\"stylename\"\:\"[a-zA-Z0-9\- ]+', str(bs))
		j =0

		for x in range(len(name)):
			name[x] = name[x].split(":")
			n = name[x][1].strip("\"")
			if product_name in n.lower() :
				key = str(j)
				d[key].append(n)
				price[x] = price[x].split(":")
				p = int(price[x][1])
				d[key].append(p)
				r = 0
				d[key].append(float(r))
				j = j+1

	elif vendor == 'flipkart' :

		product = product_name.replace(" ", "%20")
		product = product.lower()
		url = "https://www.flipkart.com/search?q=" + product_name + "&otracker=start&as-show=on&as=off"
		sess = dryscrape.Session()
		sess.visit(url)
		response = sess.body()
		soup = BeautifulSoup(response)
		bs = soup.findAll("div" , attrs = { 'class' : 'col col-3-12 col-md-3-12 MP_3W3'})
		x = 0
		if len(bs) == 0 :
			bs = soup.findAll("div" , attrs = { 'class' : 'col _2-gKeQ'})

			for i in bs :
				
				try :
					title = i.find("div", attrs = { 'class' : '_3wU53n'})
					name = title.text
				except AttributeError :
					name = "None"

				if product_name in name.lower() :
					key = str(x)
					d[key].append(name)	

					try :
						price = i.find("div", attrs = { 'class' : '_1vC4OE _2rQ-NK'})
						price = price.text
						price = re.findall(r'\d+\.?\d*', str(price))
						p = price[0]
					except AttributeError :
						p = 0
					d[key].append(float(p))
					try :
						rating = i.find("div", attrs = { 'class' : 'hGSR34 _2beYZw'})
						rating = rating.text
						rate = re.findall(r'\d+\.?\d*', str(rating))
						r = rate[0]
					except AttributeError :
						r = 0
					d[key].append(float(r))
					x = x+1
				
		else :

			for i in bs:

				try :
					name = i.find("a", attrs = { 'class' : '_2cLu-l'})
					name = name.text
				except AttributeError :
					name = 'None'

				if product_name in name.lower() :
					key = str(x)
					d[key].append(name)
					try :
						price = i.find("div", attrs = {"class" : "_1vC4OE"})
						price = price.text
				
						price = re.findall(r'\d+\.?\d*', str(price))
						p = price[0]
					except AttributeError :
						price = 0

					d[key].append(float(p))
					try :
						rating = i.find("div", attrs = {'class' : 'hGSR34 _2beYZw'})
						rating = rating.text
						rate = re.findall(r'\d+\.?\d*', str(rating))
						r = rate[0]
					except AttributeError :
						r = 0
					d[key].append(float(r))
					x = x+1


	elif vendor == 'paytm':
		product = product_name.replace(" ", "%20")
		product = product.lower()

		url = "https://paytm.com/shop/search?q=" + product + "&from=organic&child_site_id=1&site_id=1"
		response = requests.get(url, headers={'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'})
		soup = BeautifulSoup(response.content)
		bs = soup.find('div', attrs = {'class' : '_1fje'})
		try :
			name = bs.findAll('div', attrs = {'class' : '_2apC'})
			price = bs.findAll('span', attrs = {'class' : '_1kMS'})
			for i in range(4) :
				key = str(i)
				d[key].append(name[i].text)
				p = price[i].text
				p = re.findall(r'\d+\.?\d*', str(p))
				pr = p[0]
				d[key].append(float(pr))
				rating = 0
				d[key].append(rating)

		except : 
			for i in range(4) :
				key = str(i)
				d[key].append('None')
				d[key].append(0)
				rating = 0
				d[key].append(rating)



	di = OrderedDict(sorted(d.items(), key=lambda t: t[0]))
	k = -1
	similarity = []
	for x in di :
		sim = SequenceMatcher(None, product_name.lower(), di[x][0].lower()).ratio()
		similarity.append(sim)

	if len(similarity) == len(set(similarity)) :
		m = max(similarity)
		k = similarity.index(m)


	else :
		m = max(similarity)
		similar = []
		k = similarity.index(m)
		for x in range(len(similarity)) :
			if similarity[x] == m :
				similar.append(x)

		maxi = 0
		for a in similar :
			if (di[str(a)][2]) > maxi :
				maxi = di[str(a)][2]
				k = a

	pro = di[str(k)][0]
	price = di[str(k)][1]

	return pro, price	


product_name = input("Enter product name : ")
vendor = input("Enter vendor : ")
try :
	i = product_name.index('..')
	product = product_name[:i]
except :
	product = product_name
vendor = vendor.lower()
product_name = product.strip()
product_name = product_name.lower()
product, price = extract(vendor, product_name)
print("Product name is :", product)
print("Price :", price)
